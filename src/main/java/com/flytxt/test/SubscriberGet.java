package com.flytxt.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HRegionInfo;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.flytxt.configuration.NeonDatabaseConfiguration;
import com.flytxt.ostrich.avro.AvroCoDec;
import com.flytxt.ostrich.bookeeper.BookKeeperSpring;
import com.flytxt.ostrich.bookskeeper.hbase.HbaseConfig;
import com.flytxt.ostrich.datamanagement.EntityAttribute;
import com.flytxt.ostrich.datamanagement.OpType;
import com.flytxt.ostrich.subscriber.Event;
import com.flytxt.ostrich.subscriber.NeonEntity;
import com.flytxt.ostrich.subscriber.Usage;
import com.flytxt.ostrich.subscriber.fields.FieldVO;
import com.flytxt.ostrich.subscriberdecorator.Metric;
import com.flytxt.ostrich.translator.subscriber.adaptor.MetricAdaptor;

import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Import({ BookKeeperSpring.class, NeonDatabaseConfiguration.class })
@Slf4j
public class SubscriberGet implements CommandLineRunner {

    @Autowired
    private HbaseConfig hbaseConfig;

    @Autowired
    private MetricAdaptor adaptor;

    AvroCoDec codec = new AvroCoDec();

    @Override
    public void run(String... arg0) throws Exception {

        try (HConnection connection = HConnectionManager.createConnection(hbaseConfig.getConfiguration())) {

            try (HTableInterface table = connection.getTable("SUBSCRIBER")) {
                Get get = new Get(Bytes.toBytes(Long.valueOf(arg0[0])));
                get.setMaxVersions();
                Result result = table.get(get);
                System.out.println("Result :");
                System.out.println(result);

                val map = result.getFamilyMap(Bytes.toBytes("M"));
                for (byte[] q : map.keySet()) {
                    System.out.println("Q : " + Bytes.toInt(q) + ", q length : " + q.length + " , length : " + map.get(q).length);
                }
                val cells = result.getColumnCells(Bytes.toBytes("M"), Bytes.toBytes(Integer.valueOf(arg0[1])));
                System.out.println("Cell count :" + cells.size());

                for (Cell cell : cells) {
                    NeonEntity deserializeFromBytes = codec.deserializeFromBytes(cell.getValue(), new Usage());
                    System.out.println(new Metric((Usage) deserializeFromBytes));
                    System.out.println("===========================================================================================================");
                    FieldVO vo = new FieldVO("Metric3", 18781, 2, 3);
                    adaptor.updateEntityWithValue(deserializeFromBytes, vo, createMetricEntityAttribute6());
                    System.out.println(new Metric((Usage) deserializeFromBytes));
                }

            }

        }

    }

    public static void main(String[] args) {
        SpringApplication.run(SubscriberGet.class, args).close();
    }

    private static List<EntityAttribute> createMetricEntityAttribute6() {
        // "4001|402:100:1|401:1378708527564:1"
        List<EntityAttribute> entityAttributes = new ArrayList<EntityAttribute>();
        OpType.getOpType(1);
        EntityAttribute ea1 = new EntityAttribute();
        ea1.setAttributeCode(402);
        ea1.setAttributeValue("70");
        ea1.setOpType(OpType.SET);
        entityAttributes.add(ea1);

        EntityAttribute ea2 = new EntityAttribute();
        ea2.setAttributeCode(401);
        ea2.setAttributeValue("1522360800000");
        ea2.setOpType(OpType.SET);
        entityAttributes.add(ea2);
        
        
        EntityAttribute ea3 = new EntityAttribute();
        ea3.setAttributeCode(403);
        ea3.setAttributeValue("true");
        ea3.setOpType(OpType.SET);
        entityAttributes.add(ea3);
        
        return entityAttributes;
    }

}
