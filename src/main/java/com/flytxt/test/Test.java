
package com.flytxt.test;

import org.apache.hadoop.hbase.util.Bytes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test {
    
    private static byte ZERO = 0;
    
    private static long MAX = 0b0111111111111111111111111111111111111111111111111111111100000000l;

    public static void main(String[] args) {
        byte[] byteArray = { 0, 0, 0, 0, 22, 15, -72, -62 };
        byte[] byteArray2 = { 0, 0, 0, 22, 15, -72, -62, 0 };
        byte[] byteArray3 = { 0, 0, 0, 22, 14, 82, 82, 81 };
        
        long counter = 0;
        for(long i = 0 ; i < 1000; i++)
        {
            byte[] key = Bytes.toBytes(i);
            if(key[7] == ZERO){
               counter++;
            }
        }
        
        log.info("{} {} {} {} {}",MAX, counter,Bytes.toLong(byteArray3), Bytes.toLong(byteArray), Bytes.toLong(byteArray2));
    }

}
