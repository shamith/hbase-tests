package com.flytxt.test;

import org.apache.hadoop.hbase.HRegionInfo;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.flytxt.ostrich.bookeeper.BookKeeperSpring;
import com.flytxt.ostrich.bookskeeper.hbase.HbaseConfig;

import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Import(BookKeeperSpring.class)
@Slf4j
public class HbaseRegionMetaTest implements CommandLineRunner {

    @Autowired
    private HbaseConfig hbaseConfig;

    // @Autowired
    // private HtableProvider tableProvider;

    @Override
    public void run(String... arg0) throws Exception {

        log.info("{}, {}", Bytes.toBytes(-2056657158l), (int) 2238310138l);
        byte[] byteArray = { 0, 0, 0, 0, 22, 15, -72, -62 };
        byte[] byteArray2 = { 0, 0, 0, 22, 15, -72, -62, 0 };
        log.info("{} {}", Bytes.toLong(byteArray), Bytes.toLong(byteArray2));

        // ResultScanner scanner = tableProvider.getSubscriberTableFromPool().getScanner(new Scan(new byte[] {}, new byte[] { 0, 0, 0, -123, 105, -26, -6 }));
        // while (scanner.iterator().hasNext()) {
        // Result result = scanner.next();
        // log.info("Row key = {}", result.getRow());
        // try {
        // log.info("row long value = {}", Bytes.toLong(result.getRow()));
        // } catch (Exception e) {
        // log.error("Error ", e);
        // }
        // }

        try (HBaseAdmin admin = new HBaseAdmin(hbaseConfig.getConfiguration())) {
            val regions = admin.getTableRegions(TableName.valueOf("SAMPLE"));

            for (HRegionInfo region : regions) {
                try {
                    log.info("=====================================================================================================");
                    log.info("Region name {}", region.getRegionNameAsString());
                    log.info("Start key {}", region.getStartKey());
                    if (region.getStartKey().length > 0)
                        log.info("Start key as long {}", Bytes.toLong(region.getStartKey()));
                    log.info("End key {}", region.getEndKey());
                    if (region.getEndKey().length > 0)
                        log.info("End key as long {}", Bytes.toLong(region.getEndKey()));

                } catch (Exception e) {
                    log.error("error while processing region");
                    log.info("String start key {}", Bytes.toString(region.getStartKey()));
                    log.info("String end key {}", Bytes.toString(region.getEndKey()));
                }
            }
        }

    }

    public static void main(String[] args) {
        SpringApplication.run(HbaseRegionMetaTest.class, args).close();
    }

}
