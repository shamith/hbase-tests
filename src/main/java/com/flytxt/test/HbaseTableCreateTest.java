package com.flytxt.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.flytxt.ostrich.bookeeper.BookKeeperSpring;
import com.flytxt.ostrich.bookskeeper.hbase.HbaseConfig;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Import(BookKeeperSpring.class)
@Slf4j
public class HbaseTableCreateTest implements CommandLineRunner {

    public static final int TTL = 60 * 60 * 24 * 10; // 10 days

    private static byte ZERO = 0;

    private static String TABLE_NAME = "SAMPLE";

    private static byte[] FAMILY = Bytes.toBytes("O");

    private static byte[] QUALIFIER = Bytes.toBytes("Q");

    @Autowired
    private HbaseConfig hbaseConfig;

    public static void main(String[] args) throws Exception {
        SpringApplication.run(HbaseTableCreateTest.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        try (HBaseAdmin admin = new HBaseAdmin(hbaseConfig.getConfiguration());) {
            if (!admin.tableExists(TableName.valueOf(TABLE_NAME))) {
                log.info("table doesnt exist creating new");
                HTableDescriptor tableDesc = new HTableDescriptor(TableName.valueOf(TABLE_NAME));
                HColumnDescriptor col = new HColumnDescriptor("O");
                col.setTimeToLive(TTL);
                tableDesc.addFamily(col);
                admin.createTable(tableDesc);
            }
            try (HConnection connection = HConnectionManager.createConnection(hbaseConfig.getConfiguration()); 
                    HTableInterface table = connection.getTable(TableName.valueOf(TABLE_NAME))) {
                List<Put> puts = new ArrayList<Put>();
                for (long i = 0; i < Long.MAX_VALUE; i++) {
                    byte[] key = Bytes.toBytes(i);
                    if (key[7] == ZERO) {
                        Put put = new Put(key);
                        put.add(FAMILY, QUALIFIER, key);
                        puts.add(put);
                    }
                    if (puts.size() == 100) {
                        table.put(puts);
                        puts.clear();
                    }
                }
            }
        }
    }

}
